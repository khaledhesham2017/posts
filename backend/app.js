const  express = require("express");
const app = express();
const bodyParser = require("body-parser");

const mongoose = require('mongoose');
const postRouters = require('./routes/posts');

mongoose.connect( "mongodb+srv://khaled:WvYjSwhCs6Guol0d@cluster0-vvhqh.mongodb.net/node-angular?retryWrites=true").then(
  ()=>{
    console.log('connected');
  }
).catch((ri)=>{
  console.log(ri);
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended : false}));
app.use((req,res,next)=>{
  res.setHeader("Access-Control-Allow-Origin","*");
  res.setHeader("Access-Control-Allow-Headers",
    "origin, X-Requested-With, Content-Type, Accept");
  res.setHeader("Access-Control-Allow-Methods",
    "GET, POST, PATCH, PUT, DELETE , OPTIONS");
  next();
});
app.use('/api/posts',postRouters);
module.exports = app;

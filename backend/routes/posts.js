const express = require("express");
const router = express.Router();
const  Post =require("../models/post");
const  multer = require("multer");
const MIME_TYPE_MAP = {
  'image/png':'png',
  'image/jpeg':'jpeg',
  'image/jpg':'jpg'};
const  storge = multer.diskStorage({
  destination: (req,file,cb)=>
  {
    const  isValid = MIME_TYPE_MAP[file.mimeType];
    let error = new error("Invalid  name type ");
    if (isValid){
      error = null;
    }
    cb(error,'backend/images');
  },
  fileName: (req,file,cb)=> {
    const  name = file.originalname.toLocaleLowerCase().split(" ").join("-");
    const ext = MIME_TYPE_MAP[file.mimeType];
    cb(null,name+'-'+Data.now()+ext);
  }
});
router.post("",(req,res,next)=>{
  const post = new Post({
    title: req.body.title,
    content: req.body.content
  });
  post.save().then(postCreated => {
    res.status(201).json({
      message: "added successfully",
      id : postCreated._id
    })
  });

});
router.get("/:id",(req,res,next) =>{
  Post.findById(req.params.id).then(post => {
    if (post){

      res.status(200).json(post);
    } else{
      res.status(404).json({message : " post not Found"})
    }
  })
});
router.get("",(req,res,next)=>{
  Post.find().then((documents)=>{
    res.status(200).json({
      message: 'success find',
      posts: documents
    });
    console.log(documents);
  });

});
router.put("/:id", (req,res, next)=> {
  const post = new Post({
    _id: req.body.id,
    title: req.body.title,
    content: req.body.content
  });

  Post.updateOne({_id: req.params.id},post).then(result => {

    res.status(200).json({message: 'updateSuccrss'});
  });
});
router.delete("/:id", (req,res, next)=> {
  Post.deleteOne({_id: req.params.id}).then(result => {
    console.log(result);
    res.status(200).json({message: 'PostDeleted'});
  });

});
module.exports = router;

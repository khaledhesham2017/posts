import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Post} from '../posts.model';
import {PostService} from '../post.service';
import {Subscription} from 'rxjs';

// @ts-ignore
@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent  implements  OnInit, OnDestroy {
 /*@Input()*/ posts: Post[] = [];
 private  postsSub: Subscription ;
  isLoading = false;
 constructor (public  postsService: PostService) {}
onDelete(postID: string) {
   console.log(postID);
   this.postsService.deletePost(postID);
}
  ngOnInit(): void {
    this.isLoading = true;
   this.postsService.getPosts();
   this.postsSub = this.postsService.getPostUpdateListener().subscribe((posts: Post[]) => {
     this.isLoading = false;
     this.posts = posts;
     }
   );
  }

  ngOnDestroy(): void {
   this.postsSub.unsubscribe();
  }

}

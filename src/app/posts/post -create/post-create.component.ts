import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Post} from '../posts.model';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {from} from 'rxjs';
import {PostService} from '../post.service';
import {ActivatedRoute, ParamMap, RouterModule} from '@angular/router';
import {mimeType} from './mime-type.validator';

// @ts-ignore
@Component({
  selector : 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.css']
})

export class  PostCreateComponent  implements  OnInit {
  private  mode = 'create';
  private  postID: string;
   post: Post;
   isLoading = false;
   imagePreview: string;
   form: FormGroup;
  constructor (public  postsService: PostService, public rout: ActivatedRoute) {}

 // @Output() postCreated = new EventEmitter<Post>();
  ngOnInit(): void {
    this.form = new FormGroup({
      title: new FormControl(null, {
        validators: [Validators.required, Validators.minLength(3) ]
      }),
      content: new FormControl(null, {
        validators: [Validators.required],
        asyncValidators: [mimeType]
      }),
      image: new  FormControl(null, {
        validators: [Validators.required]
      })
    });

    this.rout.paramMap.subscribe((parMap: ParamMap) => {
      if (parMap.has('postId')) {
        this.mode = 'edit';
        this.postID = parMap.get('postId');
        this.isLoading = true;
        this.postsService.getPost( this.postID ).subscribe(postData =>  {
          this.isLoading = false;
          this.post = {id : postData._id, title: postData.title, content: postData.content};
          this.form.setValue({
              title: this.post.title,
              content: this.post.content
            }
          );
        });



      } else {
        this.mode = 'create';
        this.postID = null;
      }
    });
  }
  onImagePicker(event: Event) {
    const file = (event.target as HTMLInputElement).files[0];
    this.form.patchValue({image: file});
    this.form.get('image').updateValueAndValidity();
    const reader = new FileReader();
    reader.onload = () => {
      this.imagePreview = (reader.result as string);

    };
    reader.readAsDataURL(file);
  }
  onSavePost() {
  if (this.form.invalid) {
    return;
  }
  this.isLoading = true;
  if (this.mode === 'create') {
    this.postsService.addPost(this.form.value.title , this.form.value.content);
  } else {
    this.postsService.updatePost(this.postID , this.form.value.title , this.form.value.content);
  }
  this.form.reset();
}



}
